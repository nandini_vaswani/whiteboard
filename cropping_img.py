from scipy import ndimage
import math
import cv2
import numpy as np
# from google.colab.patches import cv2_imshow
# Let's load a simple image with 3 black squares
# img = cv2.imread('Screenshot (587).jpg')


# def preprocess(image):
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     blur = cv2.GaussianBlur(gray, (5, 5), 0)
#     ret3, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
#     img_edges = cv2.Canny(gray, 100, 100, apertureSize=3)
#     return thresh


def findSignificantContours(image, edgeImg):
    contours, heirarchy = cv2.findContours(edgeImg, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # Find level 1 contours
    level1 = []
    for i, tupl in enumerate(heirarchy[0]):
        # Each array is in format (Next, Prev, First child, Parent)
        # Filter the ones without parent
        if tupl[3] == -1:
            tupl = np.insert(tupl, 0, [i])
            level1.append(tupl)
    significant = []
    tooSmall = edgeImg.size * 5 / 100  # If contour isn't covering 5% of total area of image then it probably is too small
    for tupl in level1:
        contour = contours[tupl[0]]
        area = cv2.contourArea(contour)
        if area > tooSmall:
            significant.append([contour, area])

            # Draw the contour on the original image
            # cv2.drawContours(img, [contour], 0, (0,255,0),2, cv2.LINE_AA, maxLevel=1)

    significant.sort(key=lambda x: x[1])
    # print ([x[1] for x in significant]);
    # mx = (0,0,0,0)      # biggest bounding box so far
    mx_area = 0
    for cont in contours:
        x, y, w, h = cv2.boundingRect(cont)
        area = w * h
        if area > mx_area:
            mx = x, y, w, h
            mx_area = area
    x, y, w, h = mx
    # Output to files
    roi = img[y:y + h, x:x + w]
    return [[x[0] for x in significant], roi]


def crop_image(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    ret3, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    img_edges = cv2.Canny(gray, 100, 100, apertureSize=3)
    # thresh = preprocess(image)
    edgeImg_8u = np.asarray(thresh, np.uint8)

    # Find contours
    significant, roi = findSignificantContours(img, edgeImg_8u)
    # cv2_imshow(img)
    mask = thresh.copy()
    mask[mask > 0] = 0
    cv2.fillPoly(mask, significant, 255)
    # Invert mask
    mask = np.logical_not(mask)

    # Finally remove the background
    img[mask] = 0
    return roi


img = cv2.imread("Screenshot (590) - out.png")
img = crop_image(img)
cv2.imwrite('Screenshot (590).png', img)
