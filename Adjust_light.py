import cv2
import numpy as np
import os
image = cv2.imread('img1.jpg')
cv2.imshow("image", image)
cv2.waitKey(0)
# # image1 = bright_image(image)


def bright_image(image):
    # image = image.astype(float32)
    Intensity_matrix = np.ones(image.shape, dtype="uint8") * 20
    bright_image = cv2.add(image, Intensity_matrix)
    # final_image = np.array(bright_image)
    # cv2.imshow("bright", bright_image)
    # cv2.waitKey(0)0
    return bright_image


image1 = bright_image(image)
cv2.imwrite("bright.jpg", image1)
# path = ('./checkpoints/resultsfinal')
# cv2.imwrite(os.path.join(path, "bright_image"), image1)
cv2.waitKey(0)
