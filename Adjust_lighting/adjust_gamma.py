from __future__ import print_function
import numpy as np
import argparse
import cv2
from skimage.exposure import is_low_contrast
from imutils.paths import list_images
import os
import imutils
# import cv2
# from .detect_low_contrast_image import check_contrast


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values

    invGamma = 1.0 / gamma
    gamma_corrected = np.array(255 * (img / 255) ** gamma, dtype='uint8')

    table = np.array([((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)


def check_contrast(image):
    image = imutils.resize(image, width=450)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # blur the image slightly and perform edge detection
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(blurred, 30, 150)
    # initialize the text and color to indicate that the input image
    # is *not* low contrast
    text = "Low contrast: No"
    color = (0, 255, 0)
    if is_low_contrast(gray, fraction_threshold=0.35):
        # update the text and color
        text = "Low contrast: Yes"
        color = (0, 0, 255)
        # otherwise, the image is *not* low contrast, so we can continue
        # processing it
    else:
        # find contours in the edge map and find the largest one,
        # which we'll assume is the outline of our color correction
        # card
        cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        c = max(cnts, key=cv2.contourArea)
        # draw the largest contour on the image
        cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        # draw the text on the output image
    cv2.putText(image, text, (5, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.8,
                color, 2)
    # show the output image and edge map
    # path = './results'
    # cv2.imwrite(os.path.join(path, " image"), image)
    # cv2.imshow("Image.png", image)
    # cv2.imshow("Edge.png", edged)
    return text, image
    # cv2.waitKey(0)
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--input", required=True,
#                 help="path to input directory of images")
# # ap.add_argument("-t", "--thresh", type=float, default=0.35,
# #                 help="threshold for low contrast")
# args = vars(ap.parse_args())
# imagePaths = sorted(list(list_images(args["input"])))
# # loop over the image paths
# for (i, imagePaths) in enumerate(imagePaths):
#     # load the input image from disk, resize it, and convert it to
#     # grayscale
#     print("[INFO] processing image {}/{}".format(i + 1,
#                                                  len(imagePaths)))
#     image = cv2.imread(imagePaths)
# # load the original image
# original=cv2.imread(args["image"])


def adjust_light(image):
    for gamma in np.arange(0.0, 3.5, 0.5):
        # ignore when gamma is 1 (there will be no change to the image)
        if gamma == 1:
            continue
        # apply gamma correction and show the images
        gamma = gamma if gamma > 0 else 0.1
        # adjusted = adjust_gamma((image).astype(np.uint8), gamma=gamma)
        # adjusted = (adjusted.astype(np.float)) / 255
        adjusted = adjust_gamma(image, gamma=gamma)
        # cv2.putText(adjusted, "g={}".format(gamma), (10, 30),
        #             cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
        text, img = check_contrast(adjusted)
        if text == "Low contrast: No":
            break
    return img

    # cv2.imshow("Images", np.hstack([image, adjusted]))
    # cv2.waitKey(0)
# image = cv2.imread(r"C:\\Users\\DELL\\Documents\\Internship\\objects-removal\\Automated-objects-removal-inpainter\\examples\\my_small_data\\img4.jpg")
# f_image = adjust_light(image)
# cv2.imshow("image", image)
# cv2.waitKey(0)
