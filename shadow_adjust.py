import numpy as np
import cv2

image = cv2.imread('bright.jpg')
cv2.imshow("image", image)
img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
thresh2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                cv2.THRESH_BINARY, 199, 5)
cv2.imshow('Adaptive Gaussian', thresh2)
cv2.waitKey(0)
